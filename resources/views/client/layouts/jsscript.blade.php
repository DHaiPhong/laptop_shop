<script src="{{asset('client/js/jquery.min.js')}}"></script>
<script src="{{asset('client/js/popper.min.js')}}"></script>
<script src="{{asset('client/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('client/js/jquery-3.0.0.min.js')}}"></script>
<script src="{{asset('client/js/plugin.js')}}"></script>
<!-- sidebar -->
<script src="{{asset('client/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script src="{{asset('client/js/custom.js')}}"></script>
<!-- javascript -->
<script src="{{asset('client/js/owl.carousel.js')}}"></script>
<script src="https:cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "100%";
    }
    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }
</script>
