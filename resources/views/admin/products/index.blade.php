@extends('admin.layouts.app')

@section('title', 'Sản Phẩm')

@section('content')
    <div class="col-12">
        <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                    <h6 class="text-white text-capitalize ps-3">Danh Sách Sản Phẩm</h6>
                </div>
            </div>
            <div class="card-body">
                @if(session('message'))
                    <div class="text-success text-4xl">{{session('message')}}</div>
                @endif
                    <a href="{{ route('products.create') }}" class="btn btn-primary">Tạo</a>
                <div class="table-responsive p-0">
                    <table class="table table-hover align-items-center mb-0">
                        <thead>
                        <tr>
                            <th class="text-uppercase text-secondary text-md font-weight-bolder opacity-7">ID</th>
                            <th class="text-uppercase text-secondary text-md font-weight-bolder opacity-7">Hình Ảnh</th>
                            <th class="text-uppercase text-secondary text-md font-weight-bolder opacity-7 ps-2">Tên</th>
                            <th class="text-uppercase text-secondary text-md font-weight-bolder opacity-7 ps-2">Giá</th>
                            <th class="text-uppercase text-secondary text-md font-weight-bolder opacity-7 ps-2">Sale</th>
                            <th class="text-secondary opacity-7 ps-4"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($products as $item)
                        <tr>
                            <td>
                                <div class="d-flex px-2 py-1" style="margin-left: 0.5rem">
                                    <div class="d-flex flex-column justify-content-center">
                                        <h6 class="mb-0 text-md">{{$item->id}}</h6>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="col-5">
                                        <img src="{{$item->images->count() > 0 ? asset('upload/' . $item->images->first()->url) : ''}}" style="object-fit:inherit" width="150px" height="100px">
                                </div>
                            </td>
                            <td>
                                <p class="text-md font-weight-bold mb-0">{{$item->name}}</p>
                            </td>
                            <td>
                                <p class="text-md font-weight-bold mb-0">{{$item->price}}</p>
                            </td>
                            <td>
                                <p class="text-md font-weight-bold mb-0">{{$item->sale}}</p>
                            </td>
                            <td class="align-middle">
                                <a href="{{route('products.edit', $item->id)}}" class="btn btn-outline-success font-weight-bold text-xs">
                                    Sửa
                                </a>
                                <a href="{{route('products.show', $item->id)}}" class="btn btn-outline-info font-weight-bold text-xs">
                                    Xem chi tiết
                                </a>
                                <form action="{{ route('products.destroy', $item->id) }}"
                                    id="form-delete{{ $item->id }}" method="post">
                                    @csrf
                                    @method('delete')
                                </form>
                                <button class="btn btn-delete btn-danger" data-id={{ $item->id }}>Delete</button>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{$products->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

@endsection


