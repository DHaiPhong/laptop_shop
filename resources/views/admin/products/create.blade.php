@extends('admin.layouts.app')

@section('title', 'Tạo Sản Phẩm')

@section('content')
    <div class="col-12">
        <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                    <h6 class="text-white text-capitalize ps-3">Tạo Sản Phẩm</h6>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route('products.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class=" input-group-static col-5 mb-4">
                            <label>Hình ảnh</label>
                            <input type="file" accept="image/*" name="image" id="image-input" class="form-control">

                            @error('image')
                                <span class="text-danger"> {{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-5">
                            <img src="" id="show-image" width="300px" height="300px" style="object-fit: scale-down" alt="">
                        </div>
                    </div>

                    <div class="input-group input-group-static mb-4">
                        <label>Tên sản phẩm</label>
                        <input type="text" value="{{ old('name') }}" name="name" class="form-control" placeholder="Nhập tên sản phẩm">
                        @error('name')
                        <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                    <div class="input-group input-group-static mb-4">
                        <label>Giá sản phẩm</label>
                        <input type="number" name="price" value="{{ old('price') }}" class="form-control" placeholder="Nhập giá sản phẩm">
                        @error('price')
                        <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                    <div class="input-group input-group-static mb-4">
                        <label>Sale</label>
                        <input type="number" name="sale" value="{{ old('sale') }}" class="form-control" placeholder="">
                        @error('sale')
                        <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                    <div class="input-group input-group-static mb-4">
                        <label>Mô tả</label>
                        <div class="row w-100 h-100">
                            <textarea name="description" id="description" class="form-control" cols="4" rows="5"
                                style="width: 100%">{{ old('description') }} </textarea>
                        </div>
                        @error('description')
                            <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                    <input type="hidden" id="inputSize" name='sizes'>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#AddSizeModal">
                        Add size
                    </button>
                    <!-- Modal -->
                    <div class="modal fade" id="AddSizeModal" tabindex="-1" aria-labelledby="AddSizeModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="AddSizeModalLabel">Add size</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <div class="modal-body" id="AddSizeModalBody">

                                </div>
                                <div class="mt-3">
                                    <button type="button" class="btn  btn-primary btn-add-size">Add</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="input-group input-group-static mb-4">
                        <label name="group" class="ms-0">Danh mục</label>
                        <select name="category_ids[]" class="form-control" multiple>
                            @foreach ($categories as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                        @error('category_ids')
                            <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-submit btn-primary">Tạo</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.21/lodash.min.js"
        integrity="sha512-WFN04846sdKMIP5LKNphMaWzU7YpMyCU245etK3g/2ARYbPK9Ub18eG+ljU96qKRCWh+quCY7yefSmlkQw1ANQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="{{ asset('plugin/ckeditor5-build-classic/ckeditor.js') }}"></script>
    <script>
        let sizes = [{
            id: Date.now(),
            size: 'M',
            quantity: 1
        }];
    </script>
    <script src="{{ asset('admin/assets/js/product/product.js') }}"></script>
@endsection


