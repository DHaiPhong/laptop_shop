@extends('admin.layouts.app')

@section('title', 'Sửa Danh Mục')

@section('content')
    <div class="col-12">
        <div class="card my-4">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                    <h6 class="text-white text-capitalize ps-3">Sửa Danh Mục</h6>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ route('categories.update', $category->id) }}" method="post">
                    @csrf
                    @method('PUT')
                    <div class="input-group input-group-static mb-4">
                        <label>Tên Danh Mục</label>
                        <input type="text" value="{{ old('name') ?? $category->name }}" name="name" class="form-control" placeholder="Nhập tên danh mục">

                        @error('name')
                        <span class="text-danger"> {{ $message }}</span>
                        @enderror
                    </div>
                    @if ($category->childrens->count() < 1)
                    <div class="input-group input-group-static mb-4">
                        <label name="group" class="ms-0">Danh Mục Cha</label>
                        <select name="parent_id" class="form-control">
                            <option value="">Chọn Danh Mục Cha</option>
                            @foreach ($parentCategories as $item)
                            <option value="{{ $item->id }}" {{ (old('parent_id') ?? $category->parent_id) == $item->id ? 'selected' : '' }} >{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    @endif
                    <button type="submit" class="btn btn-submit btn-primary">Sửa</button>
                </form>
            </div>
        </div>
    </div>
@endsection
